## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Code review guideline

When contributing code to the docs project, you must get your code reviewed before merging the code into master.

The responsibility to find the best solution and implement it lies with the merge request author.

Before assigning a merge request to a maintainer for approval and merge, you should be confident that it actually solves the problem it was meant to solve,
that it does so in the most appropriate way, that it satisfies all requirements, and that there are no remaining bugs, logical problems, uncovered edge cases,
or known vulnerabilities. The best way to do this, and to avoid unnecessary back-and-forth with reviewers, is to perform a self-review of your own merge
request.

### Frontend

1. Once you've verified that your code is in working order, assign the MR to a maintainer (refer to the [team page](https://about.gitlab.com/company/team/) to find a frontend maintainer of the docs project).
2. The maintainer will review and merge your code.
